#include "Muralla.hpp"
#include <cstdarg>
#include <iostream>
using namespace std;

Muralla::Muralla(int nColumnas)
{
	nCol = nColumnas;
	columnas = new int[nCol];

	for (int i; i<nCol; i++)
	{
		columnas[i]= -1;
	} 

	// es la cantidad maxima de movimientos
	m_minimos = (nColumnas-1)*100;
}

Muralla::~Muralla()
{
	delete[] columnas;
}

void Muralla::setAltura(int nCol, ...)
{
	va_list lista;

	va_start(lista,nCol);
	
	for (int i=0; i<nCol; i++)
	{
		columnas[i] = va_arg(lista, int);
	}
	va_end(lista);;

}

void Muralla::setAltura(int index, int altura)
{
	if (index<nCol)
		columnas[index] = altura;
}

void Muralla::back()
{
	ordenar();

	back(0, nCol-1, 0);
}

void Muralla::back(int i, int j, int m_actuales)
{
	for (int n=i; n<nCol-1; n++)
	{
		for (int m=j; m>n; m--)
		{
			if (columnas[n] > columnas[m]+1)
			{
				columnas[n]-= 1;
				columnas[m]+= 1;

				back(n, m, m_actuales+1);

				if (esta_equilibrada() && m_actuales+1 < m_minimos )
					this->m_minimos = m_actuales+1;

				columnas[m]-=1;
				columnas[n]+=1;

			}
		}
	}
}

void Muralla::combinar(int array[], int inicio, int medio, int fin)
{
    int aux[fin-inicio+1];
	int indAux, indFst, indSnd;
	int i; 
	indAux = 0;         //<! indice del arreglo auxiliar
	indFst = inicio;    //<! indice de la primera mitad
	indSnd = medio+1;   //<! indice de la segunda mitad
	
	while (indFst <= medio && indSnd <= fin)
	{
		//  cambiar <= por >= si se quiere ordenar de mayor a menor
		if (array[indFst] >= array[indSnd])
		{
			aux[indAux++] = array[indFst++];
		}
		else
		{
			aux[indAux++] = array[indSnd++];
		}
	}

	// Se copian los elementos de la primera mitad no comparados

	while (indFst<=medio)
	{
		aux[indAux++] = array[indFst++];
	}

	// Se copian los elementos de la segunda mitad no comparados

	while (indSnd <= fin)
	{
		aux[indAux++] = array[indSnd++];
	}

	indAux = 0;

	/* Finalmente se copian los elementos del array auxiliar (ordenados)
	   en el array original */

	for (i = inicio; i <= fin; i++)
	{
		array[i] = aux[indAux++];
	}
}

void Muralla::merge_sort(int array[], int inicio, int fin)
{
	int medio = (inicio + fin)>>1; // Division por 2 menos costosa

	if (inicio < fin)
	{ 
		merge_sort(array, inicio, medio);
		merge_sort(array, medio +1, fin);
		combinar(array, inicio, medio, fin);
	}
}

void Muralla::ordenar()
{
	merge_sort(columnas, 0, nCol-1);
}

int Muralla::esta_equilibrada()
{
	for (int i=0; i<nCol-1; i++)
	{
		if (columnas[i]!=columnas[i+1])
			return 0;
	}
	return 1;
}

