#include <cstdarg>

class Muralla {

	private:
		int nCol, *columnas;
		int m_minimos;	//<! Cantidad minima de movimientos hasta ahora

		/**
		  Ejecuta el llamado recursivo a la funcion de backtracking.
		  @param i Es la columna a la cual se le estan quitando ladrillos
		  @param j Es la columna a la cual se le estan cediendo ladrillos
		  @param m_actuales Los ladrillos movidos hasta ahora.
		  */
		void back(int i, int j, int m_actuales );

		/**
		  Subfuncion de mergesort. Combina las subsoluciones del algoritmo
		  de ordenamiento por mezcla.
		  @param array Es el array original en donde se copiaran las 
		  			subsoluciones ordenadas.
		  @param inicio Es el inicio del primer subarray
		  @param medio Es el fin del primer subarray
		  @param fin Es el fin del segundo subarray
		  */
		void combinar(int array[], int inicio, int medio, int fin);
		/**
		  Ordenamiento por mezcla. Ordena un array desde inicio a fin.
		  @param array Es el array que se ordenara
		  @param inicio Es el index de inicio del array
		  @param fin Es el index de fin del array
		  */
		void merge_sort(int array[], int inicio, int fin);

	public:
		/**
		  Constructor. Crea una nueva muralla de nColumnas columnas.
		  @param nColumnas Son la cantidad de columnas de la muralla, 1 por default
		  */
		Muralla(int nColumnas=1);

		/**
		  Destructor. Libera la memoria de las torres.
		  */
		~Muralla();

		/**
		  Setea la altura de una torre. Funcion que modifica la altura de
		  la torre de la posicion index a la altura 'altura'
		  @param index Es el numero de la torre
		  @param altura Es la nueva altura
		  */
		void setAltura(int index, int altura);

		/**
		  Dadas las alturas de todas las torres, las setea al valor 
		  correspondiente.
		  @param nCol Es la cantidad de columnas
		  @param ... Son los valores de las alturas de las torres.
		  */
		void setAltura(int nCol, ...);

		/**
		  Ejecuta la primera llamada del backtrack
		  */
		void back();

		/**
		  Comprueba si la muralla esta equilibrada.
		  @return 1 Si la muralla esta equilibrada y 0 en caso contrario
		 */
		int esta_equilibrada();

		/**
		 Indica la cantidad minima de movimientos para equilibrar la
		 muralla
		 */
		int getOptimo() { return m_minimos;} 

		/**
		  Ordena la lista de columnas de mayor a menor.
		  */
		void ordenar();
};

