CC = g++
OBJ = Obj/Muralla.o Obj/main.o
WARN = 

todo: Lab2

Obj/Muralla.o: Muralla.hpp Muralla.cpp
	$(CC) -c Muralla.cpp $(WARN) -o Obj/Muralla.o

Obj/main.o: main.cpp Muralla.hpp
	$(CC) -c main.cpp $(WARN) -o Obj/main.o

Lab2: $(OBJ)
	$(CC) $(OBJ) -o bobsWall

clean:
	rm bobsWall
	rm $(OBJ)

