#include <fstream>
#include <iostream>
#include "Muralla.hpp"
#include <string>
using namespace std;

int main(int argc, char** args)
{
	int altura, nCol, cont=1;

	ifstream in(args[1]);

	string st(args[1]);
	st+=".out";

	ofstream out(st.data(), ofstream::trunc);

	if (!in.is_open())
	{
		cerr<< "No se encontro el archivo "<< args[1]<< endl;
		return 1;
	}

	in >> nCol;
	
	while (nCol!=0)
	{
		Muralla wall = Muralla(nCol);

		for (int i=0; i<nCol; i++)
		{
			in >> altura;
			wall.setAltura(i, altura);
		}

		wall.back();

		out << "Set #"<< cont++ << endl;
		out << "La cantidad minima de movimientos es "<< wall.getOptimo()<<endl;

		in >> nCol; 
	}
					
	out.close(); 
	in.close();

	return 0;
}
